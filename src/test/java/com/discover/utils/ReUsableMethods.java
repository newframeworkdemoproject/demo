package com.discover.utils;

import com.discover.pages.AuthenticationPage;
import com.discover.pages.HomePage;
import com.discover.pages.ProfilePage;
import com.discover.pages.WelcomePage;
import com.framework.engine.base.BaseTestCase;
import com.framework.engine.exception.GenericException;
import com.framework.engine.helper.AssertStep;
import com.framework.mobile.tool.MobileTestTool;
import com.t.mobile.constants.LogType;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

public class ReUsableMethods
{
    MobileTestTool ttMob;

    BaseTestCase btc;

    public ReUsableMethods(MobileTestTool mobileTestTool, BaseTestCase baseTestCase)
    {
        this.ttMob = mobileTestTool;
        this.btc = baseTestCase;
    }

    public void doValidLogin(Map<String, Object> testData) throws GenericException, IOException {
        performLoginSteps(testData);
        HomePage homePage = new HomePage(ttMob);
        ttMob.setLoggingForStep(false);
        boolean isStepPassed = homePage.isHomeVisible(true);
        logStep(isStepPassed, "Validate user able to login and app navigates to home screen.",
                "User should be able to login the app and navigates to home screen.",
                "User logged into app and navigates to home screen.",
                "User not able to login the app and navigates to home screen.");
    }

    public void doInvalidLogin(Map<String, Object> testData) throws GenericException, IOException {
        performLoginSteps(testData);
        ttMob.setLoggingForStep(false);
        AuthenticationPage authenticationPage = new AuthenticationPage(ttMob);
        boolean isStepPassed = authenticationPage.validatePinError(testData.get("ERROR").toString(), true);
        logStep(isStepPassed, "Validate the Invalid Login Message.",
                "Error message 'Enter Code Here, ERROR: Please enter a valid code.' should be displayed.",
                "Error message 'Enter Code Here, ERROR: Please enter a valid code.' is displaying on the app.",
                "Error message 'Enter Code Here, ERROR: Please enter a valid code.' is not display on the app.");
    }

    public void validateOfferInStuff(Map<String, Object> testData) throws GenericException, IOException
    {
        HomePage homePage = new HomePage(ttMob);
        String offerTitle = testData.get("OFFER_TITLE").toString();
        homePage.clickOfferView(offerTitle);
        ttMob.setLoggingForStep(true);
        homePage.clickButtonSaveToStuff();
        boolean isStepPassed = homePage.validateOfferAdded(true);
        logStep(isStepPassed, String.format("Validate offer '%s' added to 'My Stuff' and can be 'Redeemed'.", offerTitle),
                String.format("Offer '%s' should be added to my stuff and can be redeemed.", offerTitle),
                String.format("Offer '%s' is added to my stuff and can be redeemed.", offerTitle),
                String.format("Offer '%s' is not added to my stuff and can be redeemed.", offerTitle));
        homePage.clickButtonClose();
        homePage.clickTabMyStuff();
        isStepPassed = homePage.isOfferSavedToStuff(offerTitle, true);
        logStep(isStepPassed, String.format("Validate offer '%s' added to 'My Stuff' and visible on list.", offerTitle),
                String.format("Offer '%s' should be added to my stuff and visible on list.", offerTitle),
                String.format("Offer '%s' is added to my stuff and visible on list.", offerTitle),
                String.format("Offer '%s' is not added to my stuff and visible on list.", offerTitle));
    }

    private void performLoginSteps(Map<String, Object> testData) throws GenericException, IOException {
        ttMob.setLoggingForStep(false);
        WelcomePage welcomePage = new WelcomePage(ttMob);
        welcomePage.clickButtonEnterPhone();
        AuthenticationPage authenticationPage = new AuthenticationPage(ttMob);
        authenticationPage.setPhoneNumber(testData.get("PHONE_NUMBER").toString());
        authenticationPage.clickButtonSubmit();
        authenticationPage.setPin(testData.get("PIN").toString());
        authenticationPage.clickButtonFinish();
        ttMob.setLoggingForStep(true);
    }

    private void logStep(boolean isStepPassed, String description, String actualString, String expectedString, String failureString)
    {
        ttMob.setLoggingForStep(true);
        if (isStepPassed)
            AssertStep.logStep(LogType.PASS, actualString, expectedString, btc, description, true);
        else
            AssertStep.logStep(LogType.FAIL, actualString, failureString, btc, description, true);
        ttMob.setLoggingForStep(false);
    }

    public void completeYourProfile(Map<String, Object> testData) throws GenericException, IOException
    {
        try {
            ttMob.setLoggingForStep(false);
            ProfilePage profilePage = new ProfilePage(ttMob);
            profilePage.clickCompleteYourProfileButton();
            profilePage.enterFirstName(testData.get("FIRST_NAME").toString());
            profilePage.enterLastName(testData.get("LAST_NAME").toString());
            profilePage.clickBorthDay();
            profilePage.selectMonthOfBirth(testData.get("BIRTH_MONTH").toString());
            profilePage.selectDayOfBirth(testData.get("BIRTH_DAY").toString());
            profilePage.clickDoneButton();
            ttMob.setLoggingForStep(true);
            Boolean stepPassed = true;
            logStep(stepPassed, "Provide the metadata to complete the profile",
                    "Metadata to complete the profile entered successfully",
                    "Metadata to complete the profile should be entered successfully",
                    "Failed to enter the metadata to complete the User Profile.");
        }catch (Exception ex){
            Boolean stepPassed = false;
            logStep(stepPassed, "Provide the metadata to complete the profile",
                    "Metadata to complete the profile entered successfully",
                    "Metadata to complete the profile should be entered successfully",
                    "Failed to enter the metadata to complete the User Profile.");
        }
    }

    public void selectFavorites(Map<String, Object> testData, Boolean addFavs) throws GenericException, IOException
    {
        try {
            ttMob.setLoggingForStep(false);
            ProfilePage profilePage = new ProfilePage(ttMob);
            String favoriteItems = testData.get("FAV_ITEMS").toString();
            String[] favItems = favoriteItems.split(Pattern.quote("|"));
            for (String favItem : favItems) {
                profilePage.clickFavoritesItem(favItem);
            }
            profilePage.clickSaveButton();
            if (addFavs)
                profilePage.verifyFavoritesTabPresence();
            else
                profilePage.verifyPickFavButtonPresence();

            ttMob.setLoggingForStep(true);
            Boolean stepPassed = true;
            logStep(stepPassed, (addFavs?"Add ": "Remove") + " the Favorites "+(addFavs?"to ": "from")+" the User Profile.",
                    "Favorites are "+(addFavs?"added to ": "removed from")+" the User Profile.",
                    "Favorites should be "+(addFavs?"added to ": "removed from")+" the User Profile.",
                    "Failed to "+(addFavs?"add ": "remove")+" Favorites "+(addFavs?"to ": "from")+" the User Profile.");
        }
        catch (Exception ex){
            Boolean stepPassed = false;
            logStep(stepPassed, (addFavs?"Add ": "Remove") + " the Favorites "+(addFavs?"to ": "from")+" the User Profile.",
                    "Favorites are "+(addFavs?"added to ": "removed from")+" the User Profile.",
                    "Favorites should be "+(addFavs?"added to ": "removed from")+" the User Profile.",
                    "Failed to "+(addFavs?"add ": "remove")+" Favorites "+(addFavs?"to ": "from")+" the User Profile.");
        }
    }
}
