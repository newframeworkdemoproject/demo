package com.discover.tests;

import com.discover.utils.ReUsableMethods;
import com.framework.engine.annotations.JsonFilePath;
import com.framework.engine.base.BaseTestCase;
import com.framework.engine.exception.GenericException;
import com.framework.engine.helper.DataProviderFactory;
import com.framework.mobile.tool.MobileTestTool;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.Map;

public class TestLoginFunctionality extends BaseTestCase
{
    MobileTestTool __ttMob;

    ReUsableMethods methods;

    @Parameters({"mobile_device_name"})
    @BeforeClass
    public void beforeClass(ITestContext context, @Optional("") String mobile_name)
    {
        __ttMob = toolManager.getMobileTestTool().init(mobile_name);
    }

    @BeforeMethod
    public void beforeMethod(ITestContext context)
    {
        methods = new ReUsableMethods(__ttMob, this);
    }

    @JsonFilePath(path = "test-data.json")
    @Test(dataProvider = "json", dataProviderClass = DataProviderFactory.class)
    public void testInValidLogin(Map<String, Object> testData) throws GenericException, IOException
    {
        methods.doInvalidLogin(testData);
    }

    @AfterMethod
    public void afterMethod()
    {

    }

    @AfterClass
    public void afterClass()
    {
        __ttMob.quitMobileDriver();
    }
}
