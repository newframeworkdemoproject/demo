package com.discover.tests;

import com.discover.pages.ProfilePage;
import com.discover.utils.ReUsableMethods;
import com.framework.engine.annotations.JsonFilePath;
import com.framework.engine.base.BaseTestCase;
import com.framework.engine.exception.GenericException;
import com.framework.engine.helper.DataProviderFactory;
import com.framework.engine.helper.LogTestScenario;
import com.framework.mobile.tool.MobileTestTool;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.Map;

public class TestE2EFunctionality extends BaseTestCase
{
    MobileTestTool __ttMob;

    ReUsableMethods methods;
    ProfilePage profilePage;

    @Parameters({"mobile_device_name"})
    @BeforeClass
    public void beforeClass(ITestContext context, @Optional("") String mobile_name)
    {
        __ttMob = toolManager.getMobileTestTool().init(mobile_name);
    }

    @BeforeMethod
    public void beforeMethod(ITestContext context)
    {
        methods = new ReUsableMethods(__ttMob, this);
    }

    @JsonFilePath(path = "test-data.json")
    @Test(dataProvider = "json", enabled = false, dataProviderClass = DataProviderFactory.class)
    public void testValidateE2E(Map<String, Object> testData) throws GenericException, IOException
    {
        methods.doValidLogin(testData);
        methods.validateOfferInStuff(testData);
    }

    @JsonFilePath(path = "test-data.json")
    @Test(dataProvider = "json", enabled = true, dataProviderClass = DataProviderFactory.class)
    public void testCompleteProfile(Map<String, Object> testData) throws GenericException, IOException
    {
        LogTestScenario.logScenario(this, testData.get("Scenario").toString());
        methods.doValidLogin(testData);
        profilePage = new ProfilePage(__ttMob);
        __ttMob.setLoggingForStep(true);
        profilePage.clickTabProfile();
        methods.completeYourProfile(testData);
        __ttMob.setLoggingForStep(true);
        profilePage.clickCancelButton();
        profilePage.clickPickFavButton();
        methods.selectFavorites(testData, true);
        __ttMob.setLoggingForStep(true);
        profilePage.clickFavoritesTab();
        methods.selectFavorites(testData, false);
    }

    @AfterMethod
    public void afterMethod()
    {

    }

    @AfterClass
    public void afterClass()
    {
        __ttMob.quitMobileDriver();
    }
}
