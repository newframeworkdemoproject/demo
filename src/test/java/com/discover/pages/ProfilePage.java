package com.discover.pages;

import com.framework.engine.base.BasePage;
import com.framework.engine.exception.GenericException;
import com.framework.mobile.tool.Direction;
import com.framework.mobile.tool.MobileTestTool;
import org.openqa.selenium.Keys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ProfilePage extends BasePage
{
    MobileTestTool ttMob;

    public ProfilePage(MobileTestTool mobileTestTool) throws IOException
    {
        this.ttMob = mobileTestTool;
        fileInit(this.getClass().getSimpleName());
    }

    public void clickTabProfile() throws GenericException
    {
        ttMob.click("button_profile", "Profile TAB");
    }

    public void clickCompleteYourProfileButton() throws GenericException
    {
        ttMob.click("button_complete_your_profile", "Complete your Profile Button");
    }

    public void enterFirstName(String firstname) throws GenericException
    {
        ttMob.setValue("txtField_firstName", firstname, true, "First Name field");
    }

    public void enterLastName(String lastname) throws GenericException
    {
        ttMob.setValue("txtField_lastName", lastname, true, "Last Name Field");
        ttMob.pressKeyboardKey("txtField_lastName", Keys.ENTER, 1, "Last Name Field");
    }

    public void clickBorthDay() throws GenericException
    {
        ttMob.click("dropdown_birthDay", "Birthday");
    }

    public void selectMonthOfBirth(String month) throws GenericException
    {
        ttMob.setValue("spinner_month", month, false, "Month Spinner");
    }

    public void selectDayOfBirth(String day) throws GenericException
    {
        ttMob.setValue("spinner_date", day, false, "Date Spinner");
    }

    public void clickDoneButton() throws GenericException
    {
        ttMob.click("button_done", "Done Button");
    }

    public void clickCancelButton() throws GenericException
    {
        ttMob.click("button_cancel", "Cancel Button");
        ttMob.click("button_discard", "Discard Button");
    }

    public void clickSaveButton() throws GenericException
    {
        ttMob.click("button_Save", "Save Button");
    }
    public void clickFavoritesItem(String itemName) throws GenericException
    {
        Map<String, String> data = new HashMap<>();
        data.put("_itemName_", itemName);
        DYNAMIC_ATTR.set(data);
        ttMob.scrollToElement("staticText_categoryItem", Direction.DOWN, "Favorite Item " + itemName + " image");
        ttMob.click("staticText_categoryItem", "Favorite Item " + itemName + " image");
        DYNAMIC_ATTR.remove();
    }

    public void verifyFavoritesTabPresence() throws GenericException
    {
        ttMob.waitForSec(5);
        ttMob.verifyElementPresent("button_Favorites", true, "Favorites Tab");
    }

    public void verifyPickFavButtonPresence() throws GenericException
    {
        ttMob.verifyElementPresent("button_pick_favs", true, "Pick Favorites Button");
    }

    public void clickFavoritesTab() throws GenericException
    {
        ttMob.click("button_Favorites", "Favorites Tab");
    }

    public void clickPickFavButton() throws GenericException
    {
        ttMob.click("button_pick_favs", "Pick Favorites Button");
    }


}
