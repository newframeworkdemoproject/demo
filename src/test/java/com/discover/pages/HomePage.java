package com.discover.pages;

import com.framework.engine.base.BasePage;
import com.framework.engine.exception.GenericException;
import com.framework.mobile.tool.Direction;
import com.framework.mobile.tool.MobileTestTool;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HomePage extends BasePage
{
    MobileTestTool ttMob;

    public HomePage(MobileTestTool mobileTestTool) throws IOException
    {
        this.ttMob = mobileTestTool;
        fileInit(this.getClass().getSimpleName());
    }

    public boolean isHomeVisible(boolean isVisible)
    {
        ttMob.waitForElementVisibleOrInvisible("button_home", 20, isVisible, "HOME TAB");
        return ttMob.verifyElementPresent("button_home", isVisible, "HOME TAB");
    }

    public void clickTabMyStuff() throws GenericException
    {
        ttMob.click("button_my_stuff", "MY STUFF TAB");
    }

    public void clickOfferView(String offerName) throws GenericException
    {
        Map<String, String> data = new HashMap<>();
        data.put("_offer_name_", offerName);
        DYNAMIC_ATTR.set(data);
        ttMob.scrollToElement("button_offer", Direction.DOWN, offerName.toUpperCase() + "VIEW");
        ttMob.click("button_offer", offerName.toUpperCase() + "VIEW");
        DYNAMIC_ATTR.remove();
    }

    public void clickButtonSaveToStuff() throws GenericException
    {
        ttMob.waitForElementVisibleOrInvisible("button_save_to_stuff", 20, true, "SAVE TO MY STUFF BUTTON");
        ttMob.click("button_save_to_stuff", "SAVE TO MY STUFF BUTTON");
    }

    public boolean validateOfferAdded(boolean isElementPresent)
    {
        ttMob.waitForElementVisibleOrInvisible("button_redeem", 20, isElementPresent, "REDEEM BUTTON");
        return ttMob.verifyElementPresent("button_redeem", isElementPresent, "REDEEM BUTTON");
    }

    public void clickButtonClose() throws GenericException
    {
        ttMob.click("button_close", "CLOSE ICON");
    }

    public boolean isOfferSavedToStuff(String offerTitle, boolean isElementPresent)
    {
        Map<String, String> data = new HashMap<>();
        data.put("_offer_name_", offerTitle);
        DYNAMIC_ATTR.set(data);
        boolean result = ttMob.verifyElementPresent("button_offer", isElementPresent, offerTitle.toUpperCase() + "VIEW");
        DYNAMIC_ATTR.remove();
        return result;
    }
}
