package com.discover.pages;

import com.framework.engine.base.BasePage;
import com.framework.engine.exception.GenericException;
import com.framework.mobile.tool.MobileTestTool;

import java.io.IOException;

public class WelcomePage extends BasePage
{
    MobileTestTool ttMob;

    public WelcomePage(MobileTestTool mobileTestTool) throws IOException {
        this.ttMob = mobileTestTool;
        fileInit(this.getClass().getSimpleName());
    }

    public boolean validateDescriptionText(String descriptionValue, boolean isValueExist)
    {
        return ttMob.verifyElementAttributeValue("text_welcome_description", "value", descriptionValue, isValueExist, "WELCOME DESCRIPTION TEXT");
    }

    public boolean validateLegalText(String legalValue, boolean isValueExist)
    {
        return ttMob.verifyElementAttributeValue("text_welcome_legal", "value", legalValue, isValueExist, "WELCOME LEGAL TEXT");
    }

    public void clickButtonLearnMore() throws GenericException
    {
        ttMob.click("button_learn_more", "LEARN MORE BUTTON");
    }

    public void clickButtonEnterPhone() throws GenericException
    {
        ttMob.click("button_enter_phone", "ENTER PHONE NUMBER BUTTON");
    }
}
