package com.discover.pages;

import com.framework.engine.base.BasePage;
import com.framework.engine.exception.GenericException;
import com.framework.mobile.tool.MobileTestTool;

import java.io.IOException;

public class AuthenticationPage extends BasePage
{
    MobileTestTool ttMob;

    public AuthenticationPage(MobileTestTool mobileTestTool) throws IOException
    {
        this.ttMob = mobileTestTool;
        fileInit(this.getClass().getSimpleName());
    }

    public void clickButtonClose() throws GenericException
    {
        ttMob.click("image_close", "CLOSE BUTTON");
    }

    public void setPhoneNumber(String phoneNumber) throws GenericException
    {
        ttMob.setValue("input_phone", phoneNumber, true, "ENTER PHONE NUMBER TEXT-FIELD");
    }

    public void clickButtonSubmit() throws GenericException
    {
        ttMob.click("button_submit", "SUBMIT BUTTON");
    }

    public void clickButtonBack() throws GenericException
    {
        ttMob.click("image_back", "BACK BUTTON");
    }

    public boolean validatePinTitle(String pinTitle, boolean isValueExist)
    {
        return ttMob.verifyElementAttributeValue("text_pin_title", "value", pinTitle, isValueExist, "PIN TITLE TEXT");
    }

    public boolean validatePinDescription(String pinDescription, boolean isValueExist)
    {
        return ttMob.verifyElementAttributeValue("text_pin_description", "value", pinDescription, isValueExist, "PIN DESCRIPTION TEXT");
    }

    public void setPin(String pinValue) throws GenericException
    {
        ttMob.waitForElementVisibleOrInvisible("input_pin", 20, true, "ENTER PIN TEXT-FIELD");
        ttMob.setValue("input_pin", pinValue, true, "ENTER PIN TEXT-FIELD");
    }

    public void clickButtonResend() throws GenericException
    {
        ttMob.click("button_resend", "RESEND BUTTON");
    }

    public void clickButtonFinish() throws GenericException
    {
        ttMob.click("button_finish", "FINISH BUTTON");
    }

    public boolean validatePinError(String pinError, boolean isValueExist)
    {
        return ttMob.verifyElementAttributeValue("input_pin", "label", pinError, isValueExist, "PIN ERROR LABEL");
    }
}
